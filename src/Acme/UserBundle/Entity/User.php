<?php
// src/Acme/UserBundle/Entity/User.php
/*
    The registration fields will be (all required): 
    • Email address 
    • Email address type (e.g. home, work, other) 
    • Password 
    • Confirm password 
    • First Name 
    • Last Name 
    • Gender 
    • Members’ picture (optional)
*/


namespace Acme\UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
         * @var string
         *
         * @ORM\Column(name="facebookId", type="string", length=255)
         */
    protected $facebookId;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Assert\NotBlank(message="Please enter your first name.", groups={"Registration", "Profile"})
     */
    protected $firstName;
    
    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Assert\NotBlank(message="Please enter your last name.", groups={"Registration", "Profile"})
     */
    protected $lastName;
    
    /**
     * @ORM\ManyToOne(targetEntity="Gender", inversedBy="users")
     * @ORM\JoinColumn(name="gender", referencedColumnName="id")
     */
    protected $gender;
    
    
    /**
    * @ORM\Column(type="date", nullable=true)
    */
    protected $birthday;
    
    /**
    * @ORM\Column(type="text")
    */
    protected $fbdataDebug;
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    public function serialize()
    {
        return serialize(array($this->facebookId, parent::serialize()));
    }

    public function unserialize($data)
    {
        list($this->facebookId, $parentData) = unserialize($data);
        parent::unserialize($parentData);
    }
    
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    public function getLastName()
    {
        return $this->lastName;
    }
    
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }
    
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }
    
    /**
     * Get the full name of the user (first + last name)
     * @return string
     */
    public function getFullName()
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }
    
    public function getGender()
    {
        return $this->gender;
    }
    
    public function setGender($gender='')
    {
        $this->gender = $gender;
    }
    
    public function getBirthday()
    {
        return $this->birthday;
    }
    
    public function setBirthday($value='')
    {
        $this->birthday = new \DateTime($value);
    }
    
    public function getFbdataDebug()
    {
        return $this->fbdataDebug;
    }
    
    public function setFbdataDebug($value='')
    {
        $this->fbdataDebug = $value;
    }
    
    /**
     * @param string $facebookId
     * @return void
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
        $this->setUsername($facebookId);
        $this->salt = '';
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param Array
     */
    public function setFBData($fbdata)
    {        
        if (isset($fbdata['id'])) {
            $this->setFacebookId($fbdata['id']);
            $this->addRole('ROLE_FACEBOOK');
        }
        if (isset($fbdata['first_name'])) {
            $this->setFirstname($fbdata['first_name']);
        }
        if (isset($fbdata['last_name'])) {
            $this->setLastname($fbdata['last_name']);
        }
        if (isset($fbdata['email'])) {
            $this->setEmail($fbdata['email']);
        }
        
        if (isset($fbdata['birthday'])) {
            $this->setBirthday($fbdata['birthday']);
        }
        
        $this->setFbdataDebug(print_r($fbdata, true));
    }
    
}