<?php

namespace Acme\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AcmeUserBundle:Default:index.html.twig', array('name' => $name));
    }
    
    /**
    * @Route("/users/search/", name="user_search")
    * @Template
    */
    public function searchAction(Request $request)
    {
        if($request->isMethod('POST')) {
            $finder = $this->get('foq_elastica.index.website.user');
            $searchTerm = $request->get('search');
            $users = $finder->search($searchTerm);
        } else {
            $users = array();
        }
        return array('users' => $users);
    }
}
